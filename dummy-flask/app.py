import traceback

from flask import Flask


app = Flask(__name__)


@app.post("/")
def predict():
    try:
        return {"status": "success!"}
    except Exception as e:
        tb = traceback.format_exc()
        return {"errorMessages": tb.replace("\n","")}


if __name__ == '__main__':
    app.run(host="0.0.0.0")
